#!/usr/bin/env bash
# fail if any commands fails
set -e
# debug log
set -x

cd "$(dirname "$0")"
PROJECT_DIR=".."

source ./core.sh

VERSION_CODE=$1

setAndroidVersions $APP_VERSION $VERSION_CODE

envman add --key ANDROID_VERSION_NAME --value $APP_VERSION
envman add --key ANDROID_VERSION_CODE --value $VERSION_CODE