#!/usr/bin/env bash
# fail if any commands fails
set -e
# debug log
set -x

cd "$(dirname "$0")"
PROJECT_DIR=".."

source ./core.sh

BUILD_NUMBER=$1
GIT_COMMIT_HASH=$2
GIT_COMMIT_HASH_SHORT=${GIT_COMMIT_HASH:0:8}
PLATFORM_NAME=$3

BUILD_ID="$APP_VERSION-$BUILD_NUMBER-$GIT_COMMIT_HASH_SHORT-$PLATFORM_NAME"

writeBuildFiles $BUILD_ID $APP_NAME