#!/usr/bin/env bash
# fail if any commands fails
set -e
# debug log
set -x

cd "$(dirname "$0")"
PROJECT_DIR=".."

source ./core.sh

RELEASE_TYPE=$1

case "$RELEASE_TYPE" in
    release)
        NEW_VERSION="$((RELEASE_VERSION+1)).0"
        ;;
    
    patch)
        NEW_VERSION="$RELEASE_VERSION.$((PATCH_VERSION+1))"
esac

# update ios version
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $NEW_VERSION" "../ios/Covid19Attestation/Info.plist"

# update android version
setAndroidVersions $NEW_VERSION

# update package.json version
# this version needs to be semver complient
# so set the patch version to 0
yarn version --no-git-tag-version --new-version "$NEW_VERSION.0"