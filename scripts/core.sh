#!/usr/bin/env bash

PACKAGE_JSON_VERSION=$(grep "version" $PROJECT_DIR/package.json | cut -d '"' -f4)

# yarn requires a semver complient version
# we only care about the first two components
# parse them from the package.json version
RELEASE_VERSION=$(echo $PACKAGE_JSON_VERSION | cut -d "." -f1)
PATCH_VERSION=$(echo $PACKAGE_JSON_VERSION | cut -d "." -f2)
APP_VERSION="$RELEASE_VERSION.$PATCH_VERSION"

setAndroidVersions() {
	NEW_VERSION=$1
	BUILD_NUMBER=${2-1}

	# update android version
	cat <<-EOF > "$PROJECT_DIR/android/app/version.properties"
	android.versionName=$NEW_VERSION
	android.versionCode=$BUILD_NUMBER
	EOF
}

writeBuildFiles() {
	APP_KEY=$2

	cat <<-EOF > "$PROJECT_DIR/build.json"
	{"buildId": "$1"}
	EOF

	envman add --key APP_GENERATED_BUILD_ID --value $1
}