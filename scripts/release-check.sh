#!/usr/bin/env bash

DANGER_PATHS=("^ios/" "^android/")
NEED_CHECK_PATHS=("^patches/")
RED="\033[0;31m"
AMBER="\033[0;33m"
GREEN="\033[0;32m"
CLEAR_COLOR="\033[0m"

COUNT_COMMAND="sed '/^\s*$/d' | wc | awk '{print $1}'"

START_REVISION=$1

echo "Checking commit history to determine release type..."

FILES_CHANGED_COUNT=$(git diff --name-only HEAD $START_REVISION | sed '/^\s*$/d' | wc | awk '{print $1}')
echo "$FILES_CHANGED_COUNT files changed since git revision $START_REVISION"

DANGER_FILES_CHANGED=$(git diff --name-only HEAD $START_REVISION | grep ${DANGER_PATHS[@]/#/-e })
DANGER_FILES_CHANGED_COUNT=$(echo "$DANGER_FILES_CHANGED" | sed '/^\s*$/d' | wc | awk '{print $1}')

if [[ "$DANGER_FILES_CHANGED_COUNT" != "0" ]]; then
    printf "${RED}✗ The following files have changed, you probably need to do an App Store release:\n\n"
    echo "$DANGER_FILES_CHANGED"
    printf "\n${CLEAR_COLOR}"
    exit 1
fi

NEED_CHECK_FILES_CHANGED=$(git diff --name-only HEAD $START_REVISION | grep ${NEED_CHECK_PATHS[@]/#/-e })
NEED_CHECK_FILES_CHANGED_COUNT=$(echo "$NEED_CHECK_FILES_CHANGED" | sed '/^\s*$/d' | wc | awk '{print $1}')

if [[ "$NEED_CHECK_FILES_CHANGED_COUNT" != "0" ]]; then
    printf "${AMBER}✗ The following files have changed, check to see that they have not introduced any binary changes. If the changes are just javascript, then you are free to do a Code Push release\n\n"
    echo "$NEED_CHECK_FILES_CHANGED"
    printf "\n${CLEAR_COLOR}"
    exit 1
fi

printf "${GREEN}✓ No native files have changed, it's probably OK to do a Code Push release"
printf "\n${CLEAR_COLOR}"
exit 0