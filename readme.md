# Covid-19 Attestation Document Generator

An app to generate an attestation needed for exiting your house in France during the COVID-19 pandemic.

Built using React Native, Typescript and React Navigation.

## How to run

* Install `Node.js` and `Yarn`
* Install the React Native CLI: `yarn global add @react-native-community/cli`
* Install the app dependencies: `yarn`
* Install CocoaPods by following instructions here: `https://cocoapods.org/#install`
* Install ios dependencies with CocoaPods: `cd ios; pod install; cd ..;`
* Run the ios app: `yarn ios` (only works on an Apple computer)
* Run the android app: `yarn android` (only works with the anddroid toolchain installed)