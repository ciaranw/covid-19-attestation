import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import MainNavigator from './main';
import Attestation from '../screens/attestation';
import Signature from '../screens/signature';

import {RootStackParamList} from './types';

const RootStack = createStackNavigator<RootStackParamList>();

export default function RootNavigator() {
    return (
        <RootStack.Navigator mode="modal">
            <RootStack.Screen
                name="Main"
                component={MainNavigator}
                options={{headerShown: false}}
            />
            <RootStack.Screen
                name="Attestation"
                component={Attestation}
                options={{
                    headerBackTitleVisible: false,
                }}
            />
            <RootStack.Screen
                name="Signature"
                component={Signature}
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                }}
            />
        </RootStack.Navigator>
    );
}
