export type MainStackParamList = {
    Startup: undefined;
    UserDetails: undefined;
    Generate: undefined;
};

export type RootStackParamList = {
    Main: undefined;
    Attestation: undefined;
    Signature: undefined;
};

export type AllStackParamList = RootStackParamList & MainStackParamList;
