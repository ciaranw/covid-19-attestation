import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {MainStackParamList} from './types';

import Startup from '../screens/startup';
import UserDetails from '../screens/user-details';
import Generate from '../screens/generate';

import HeaderButton from '../components/header-button';

import {translate} from '../i18n/index';

const MainStack = createStackNavigator<MainStackParamList>();

export default function MainNavigator() {
    return (
        <MainStack.Navigator>
            <MainStack.Screen
                name="Startup"
                component={Startup}
                options={{headerShown: false}}
            />
            <MainStack.Screen
                name="UserDetails"
                component={UserDetails}
                options={{title: translate('UserDetails')('title').t()}}
            />
            <MainStack.Screen
                name="Generate"
                component={Generate}
                options={({navigation}) => ({
                    title: translate('Generate')('title').t(),
                    headerRight: () => (
                        <HeaderButton
                            iconName="user-circle-o"
                            onPress={() => navigation.navigate('UserDetails')}
                        />
                    ),
                })}
            />
        </MainStack.Navigator>
    );
}
