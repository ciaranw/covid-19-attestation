import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import Orientation from 'react-native-orientation-locker';

import ScreenContainer from '../components/screen-container';
import Heading from '../components/heading';

import {AllStackParamList} from '../navigation/types';
import {State} from '../redux/index';

type StartupProps = {
    navigation: StackNavigationProp<AllStackParamList, 'Startup'>;
};

const Startup: React.FC<StartupProps> = ({navigation}) => {
    const hasUserDetails = useSelector<State, boolean>(
        state =>
            state.UserDetails.name !== '' &&
            state.UserDetails.address !== '' &&
            state.UserDetails.dob !== '',
    );

    React.useEffect(() => {
        Orientation.lockToPortrait();
        if (!hasUserDetails) {
            navigation.replace('UserDetails');
        } else {
            navigation.replace('Generate');
        }
    }, [hasUserDetails, navigation]);
    return (
        <ScreenContainer>
            <Heading>COVID-19</Heading>
            <Heading>Attestation</Heading>
        </ScreenContainer>
    );
};

export default Startup;
