import React from 'react';
import {Text, View, ScrollView, Image} from 'react-native';
import {useSelector} from 'react-redux';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';
import {format} from 'date-fns';

import ScreenContainer from '../components/screen-container';
import Heading from '../components/heading';
import AttestationField from '../components/attestation/field';
import TripReasonCheckbox from '../components/trip-reason';

import {State} from '../redux/index';
import {UserDetailsState} from '../redux/user-details';
import {ActivityState} from '../redux/activity';

import frTranslations from '../i18n/fr';
const activityTypes = frTranslations.Generate.activities;
type ActivityTypes = keyof typeof activityTypes;

const AttestationHeading = styled(Heading)`
    font-size: ${scale(12)}px;
    font-weight: 800;
    margin-bottom: ${scale(10)}px;
`;

const AttestationSubtitle = styled(Text)`
    font-size: ${scale(12)}px;
    text-align: center;
    margin-bottom: ${scale(30)}px;
`;

const AttestationActivityHeading = styled(Text)`
    font-size: ${scale(12)}px;
    margin-bottom: ${scale(10)}px;
    text-align: justify;
`;

const AttestationDateContainer = styled(View)`
    align-self: flex-end;
    align-items: flex-end;
    margin-top: ${scale(10)}px;
    margin-bottom: ${scale(30)}px;
`;

const AttestationDate = styled(Text)`
    font-size: ${scale(12)}px;
`;

const AttestationSignature = styled(Image)`
    width: ${scale(300)}px;
    height: ${scale(120)}px;
`;

type AttestationSelector = {
    userDetails: UserDetailsState;
    signature: string;
    activity: ActivityState;
};

export default function Attestation() {
    const state = useSelector<State, AttestationSelector>(state => ({
        userDetails: state.UserDetails,
        signature: state.Signature,
        activity: state.Activity,
    }));

    const renderCheckbox = (id: ActivityTypes) => (
        <TripReasonCheckbox
            checked={id === state.activity.selected}
            changeSelectedStyle={false}>
            {frTranslations.Generate.activities[id]}
        </TripReasonCheckbox>
    );

    return (
        <ScrollView>
            <ScreenContainer>
                <AttestationHeading>
                    ATTESTATION DE DÉPLACEMENT DÉROGATOIRE
                </AttestationHeading>
                <AttestationSubtitle>
                    En application de l’article 1er du décret du 16 mars 2020
                    portant réglementation des déplacements dans le cadre de la
                    lutte contre la propagation du virus Covid-19 :
                </AttestationSubtitle>
                <AttestationField label="Je soussigné(e)" />
                <AttestationField
                    label="Mme / M."
                    value={state.userDetails.name}
                />
                <AttestationField
                    label="Né(e) le :"
                    value={state.userDetails.dob}
                />
                <AttestationField
                    label="Demeurant :"
                    value={state.userDetails.address}
                />
                <AttestationActivityHeading>
                    certifie que mon déplacement est lié au motif suivant
                    (cocher la case) autorisé par l’article 1er du décret du 16
                    mars 2020 portant réglementation des déplacements dans le
                    cadre de la lutte contre la propagation du virus Covid-19 :
                </AttestationActivityHeading>
                {renderCheckbox('work')}
                {renderCheckbox('shops')}
                {renderCheckbox('health')}
                {renderCheckbox('family')}
                {renderCheckbox('exercise')}
                <AttestationDateContainer>
                    <AttestationDate>
                        le {format(new Date(), 'dd/MM/yyyy')}
                    </AttestationDate>
                    <AttestationDate>(signature)</AttestationDate>
                    <AttestationSignature
                        source={{uri: state.signature}}
                        resizeMode="contain"
                    />
                </AttestationDateContainer>
            </ScreenContainer>
        </ScrollView>
    );
}
