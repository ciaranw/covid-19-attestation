import React from 'react';
import {Button, Alert} from 'react-native';
import styled from 'styled-components';
import {useSelector, useDispatch} from 'react-redux';
import {StackNavigationProp} from '@react-navigation/stack';
import {scale} from 'react-native-size-matters';

import ScreenContainer from '../components/screen-container';
import Heading from '../components/heading';
import TripReasonCheckbox from '../components/trip-reason';
import Spacer from '../components/spacer';

import {translate} from '../i18n/index';
import {State} from '../redux/index';
import {update, ActivityState} from '../redux/activity';
import {AllStackParamList} from '../navigation/types';

const GenerateScreenContainer = styled(ScreenContainer)`
    align-items: stretch;
    justify-content: flex-start;
    padding-bottom: ${scale(10)}px;
`;

type GenerateProps = {
    navigation: StackNavigationProp<AllStackParamList, 'Generate'>;
};

type Activities = 'work' | 'shops' | 'health' | 'family' | 'exercise';

const Generate: React.FC<GenerateProps> = ({navigation}) => {
    const selectedId = useSelector<State, ActivityState['selected']>(
        state => state.Activity.selected,
    );
    const dispatch = useDispatch();

    const isSelected = (id: string) => id === selectedId;
    const setSelected = (id: string) => () => dispatch(update(id));

    const renderCheckbox = (id: Activities) => (
        <TripReasonCheckbox checked={isSelected(id)} onPress={setSelected(id)}>
            {translate('Generate')('activities')(id).t()}
        </TripReasonCheckbox>
    );

    const onPressGenerate = () => {
        if (selectedId == null) {
            Alert.alert(translate('Generate')('missingActivity').t());
        } else {
            Alert.alert(
                translate('Generate')('warning')('title').t(),
                translate('Generate')('warning')('message').t(),
                [
                    {
                        text: translate('Generate')('warning')('ok').t(),
                        onPress: () => navigation.navigate('Attestation'),
                    },
                    {
                        text: translate('Generate')('warning')('cancel').t(),
                        style: 'cancel',
                    },
                ],
            );
        }
    };

    return (
        <GenerateScreenContainer>
            <Heading>{translate('Generate')('heading').t()}</Heading>
            {renderCheckbox('work')}
            {renderCheckbox('shops')}
            {renderCheckbox('health')}
            {renderCheckbox('family')}
            {renderCheckbox('exercise')}
            <Spacer />
            <Button
                title={translate('Generate')('title').t()}
                onPress={onPressGenerate}
            />
        </GenerateScreenContainer>
    );
};

export default Generate;
