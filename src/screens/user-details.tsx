import React from 'react';
import styled from 'styled-components';
import {useDispatch, useSelector} from 'react-redux';
import {StackNavigationProp} from '@react-navigation/stack';

import ScreenContainer from '../components/screen-container';
import UserDetailsForm from '../components/user-details-form';
import Heading from '../components/heading';

import {translate} from '../i18n/index';
import {State} from '../redux/index';
import {update, UserDetailsState} from '../redux/user-details';
import {AllStackParamList} from '../navigation/types';

const UserDetailsScreenContainer = styled(ScreenContainer)`
    align-items: stretch;
    justify-content: flex-start;
`;

type UserDetailsProps = {
    navigation: StackNavigationProp<AllStackParamList, 'UserDetails'>;
};

const UserDetails: React.FC<UserDetailsProps> = ({navigation}) => {
    const dispatch = useDispatch();
    const signature = useSelector<State, string>(state => state.Signature);

    const onSubmit = (values: UserDetailsState) => {
        if (signature === '') {
            onAddSignature();
            return;
        }

        dispatch(update(values));
        navigation.reset({
            index: 0,
            routes: [{name: 'Generate'}],
        });
    };

    const onAddSignature = () => {
        navigation.navigate('Signature');
    };

    return (
        <UserDetailsScreenContainer>
            <Heading>{translate('UserDetails')('heading').t()}</Heading>
            <UserDetailsForm
                onSubmit={onSubmit}
                onAddSignature={onAddSignature}
            />
        </UserDetailsScreenContainer>
    );
};

export default UserDetails;
