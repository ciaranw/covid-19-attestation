import React from 'react';
import {SafeAreaView} from 'react-native';
import SignatureCanvas from 'react-native-signature-canvas';
import styled from 'styled-components';
import Orientation from 'react-native-orientation-locker';
import {StackNavigationProp} from '@react-navigation/stack';
import {useDispatch} from 'react-redux';

import {AllStackParamList} from '../navigation/types';
import {translate} from '../i18n/index';
import {update} from '../redux/signature';

type SignatureProps = {
    navigation: StackNavigationProp<AllStackParamList, 'Signature'>;
};

const SignatureContainer = styled(SafeAreaView)`
    flex: 1;
`;

const Signature: React.FC<SignatureProps> = ({navigation}) => {
    React.useEffect(() => {
        Orientation.lockToLandscapeLeft();

        return () => Orientation.lockToPortrait();
    }, []);

    const dispatch = useDispatch();

    const close = () => {
        Orientation.unlockAllOrientations();
        navigation.goBack();
    };

    const onOk = (signatureImg: string) => {
        dispatch(update(signatureImg));
        close();
    };

    return (
        <SignatureContainer>
            <SignatureCanvas
                onOK={onOk}
                onEmpty={close}
                descriptionText={translate('Signature')('title').t()}
                confirmText={translate('Signature')('done').t()}
                clearText={translate('Signature')('clear').t()}
            />
        </SignatureContainer>
    );
};

export default Signature;
