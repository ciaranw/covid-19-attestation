import * as t from 'io-ts';

const validate = (input: unknown) => input != null && input !== '';

const nonEmptyString = new t.Type<string, string, unknown>(
    'non-empty-string',
    (input: unknown): input is string => {
        return validate(input);
    },
    (input, context) => {
        if (validate(input)) {
            return t.success(input as string);
        } else {
            return t.failure(input, context);
        }
    },
    t.identity,
);

export default nonEmptyString;
