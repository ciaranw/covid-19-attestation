import * as t from 'io-ts';
import {parse, format, isValid} from 'date-fns';

const DATE_FORMAT = 'dd/MM/yyyy';

export function parseDate(input: any): Date | null {
    if (typeof input !== 'string' || input == null) {
        return null;
    }
    try {
        const date = parse(input, DATE_FORMAT, new Date());
        if (isValid(date)) {
            return date;
        } else {
            return null;
        }
    } catch (err) {
        return null;
    }
}

const stringDate = new t.Type<string, string, unknown>(
    'string-date',
    (input: unknown): input is string => {
        const result = parseDate(input);
        return result != null;
    },
    (input, context) => {
        const result = parseDate(input);
        if (result == null) {
            return t.failure(input, context);
        } else {
            return t.success(input as string);
        }
    },
    t.identity,
);

export const formatDate = (date: Date) => format(date, DATE_FORMAT);

export default stringDate;
