import * as t from 'io-ts';

import date from './date';
import nonEmptyString from './non-empty-string';

export default t.type({
    name: nonEmptyString,
    address: nonEmptyString,
    dob: date,
});
