import {View} from 'react-native';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';
import {TypeC, Validation} from 'io-ts';
import {pipe} from 'fp-ts/lib/pipeable';
import {fold} from 'fp-ts/lib/Either';

import {translate} from '../i18n/index';

export const FormContainer = styled(View)`
    flex: 1;
    align-items: stretch;
    padding-vertical: ${scale(10)}px;
`;

const formikReporter = (
    validation: Validation<any>,
): {[key: string]: string} => {
    return pipe(
        validation,
        fold(
            errors =>
                errors.reduce((errorMap, error) => {
                    const errorKey = error.context
                        .filter(({key}) => key !== '')
                        .map(({key}) => key)
                        .join('.');
                    return {
                        ...errorMap,
                        [errorKey]: translate('components')('FormError')(
                            'invalid',
                        ).t(),
                    };
                }, {}),
            () => ({}),
        ),
    );
};

export const validateFormModel = (model: TypeC<any>) => {
    return (values: any) => formikReporter(model.decode(values));
};
