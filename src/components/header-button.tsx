import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styled from 'styled-components';

const ButtonContainer = styled(View)`
    padding-horizontal: 10px;
`;

type HeaderButtonProps = {
    onPress?: () => void;
    iconName: string;
};

const HeaderButton: React.FC<HeaderButtonProps> = ({onPress, iconName}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <ButtonContainer>
                <Icon name={iconName} size={24} />
            </ButtonContainer>
        </TouchableOpacity>
    );
};

export default HeaderButton;
