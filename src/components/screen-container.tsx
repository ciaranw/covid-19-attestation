import {View} from 'react-native';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';

export default styled(View)`
    flex: 1;
    align-items: center;
    justify-content: center;
    margin: ${scale(10)}px;
`;
