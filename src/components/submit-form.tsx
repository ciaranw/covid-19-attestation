import React from 'react';
import {Button, ButtonProperties} from 'react-native';
import {useFormikContext} from 'formik';

type SubmitFormProps = Omit<ButtonProperties, 'onPress'>;

const SubmitForm: React.FC<SubmitFormProps> = props => {
    const {submitForm} = useFormikContext();

    return <Button {...props} onPress={() => submitForm()} />;
};

export default SubmitForm;
