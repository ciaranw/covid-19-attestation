import React from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';

type CheckboxContainerProps = {
    checked: boolean;
};

const CheckboxContainer = styled(View)<CheckboxContainerProps>`
    padding: ${scale(10)}px;
    align-items: center;
    background-color: ${props =>
        props.checked ? 'rgb(158, 223, 240)' : 'transparent'};
    flex-direction: row;
`;

const StyledIcon = styled(Icon)`
    width: ${scale(30)}px;
`;

const CheckboxText = styled(Text)`
    flex: 1;
    font-size: ${scale(12)}px;
`;

type TripReasonCheckboxProps = {
    checked: boolean;
    onPress?: () => void;
    children: string;
    changeSelectedStyle?: boolean;
};

const TripReasonCheckbox: React.FC<TripReasonCheckboxProps> = ({
    checked,
    onPress,
    children,
    changeSelectedStyle = true,
}) => {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <CheckboxContainer checked={changeSelectedStyle ? checked : false}>
                <StyledIcon
                    name={checked ? 'check-square-o' : 'square-o'}
                    size={scale(24)}
                />
                <CheckboxText>{children}</CheckboxText>
            </CheckboxContainer>
        </TouchableWithoutFeedback>
    );
};

export default TripReasonCheckbox;
