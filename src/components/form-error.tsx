import React from 'react';
import {View, Text} from 'react-native';
import {ErrorMessage} from 'formik';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';

const ErrorContainer = styled(View)`
    margin-bottom: ${scale(15)}px;
`;

const ErrorText = styled(Text)`
    color: red;
    font-size: ${scale(10)}px;
`;

type FormErrorProps = {
    name: string;
};

const FormError: React.FC<FormErrorProps> = ({name}) => (
    <ErrorMessage
        name={name}
        render={msg => (
            <ErrorContainer>
                <ErrorText>{msg}</ErrorText>
            </ErrorContainer>
        )}
    />
);

export default FormError;
