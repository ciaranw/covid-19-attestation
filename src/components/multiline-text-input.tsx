import React from 'react';
import {
    View,
    Button,
    InputAccessoryView,
    Keyboard,
    Platform,
} from 'react-native';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';

import TextInput, {TextInputProps} from './text-input';
import {translate} from '../i18n/index';

const AccsesoryViewContainer = styled(View)`
    background-color: rgb(222, 222, 222);
    padding: ${scale(5)}px;
    align-items: flex-end;
    border-color: black;
    border-top-width: 0.5px;
`;

type MultilineTextInputProps = Omit<TextInputProps, 'multiline'>;

const INPUT_ACCESSORY_VIEW_ID = 'multiline-text-input';

const MultilineTextInput: React.FC<MultilineTextInputProps> = props => {
    const closeKeyboard = () => Keyboard.dismiss();

    return (
        <>
            <TextInput
                multiline={true}
                inputAccessoryViewID={INPUT_ACCESSORY_VIEW_ID}
                {...props}
            />
            {Platform.OS === 'ios' && (
                <InputAccessoryView nativeID={INPUT_ACCESSORY_VIEW_ID}>
                    <AccsesoryViewContainer>
                        <Button
                            onPress={closeKeyboard}
                            title={translate('components')(
                                'MultilineTextInput',
                            )('done').t()}
                        />
                    </AccsesoryViewContainer>
                </InputAccessoryView>
            )}
        </>
    );
};

export default MultilineTextInput;
