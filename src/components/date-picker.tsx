import React from 'react';
import {TouchableOpacity} from 'react-native';
import DateTimePickerModal, {
    DateTimePickerProps,
} from 'react-native-modal-datetime-picker';
import {useFormikContext} from 'formik';

import {getLanguageTag} from '../i18n/index';
import TextInput from './text-input';
import {formatDate, parseDate} from '../models/date';

type DatePickerProps = {
    name: string;
    placeholder?: string;
} & Omit<DateTimePickerProps, 'minuteInterval' | 'onConfirm' | 'onCancel'>;

const DatePicker: React.FC<DatePickerProps> = ({
    name,
    placeholder,
    ...datePickerProps
}) => {
    const [showingPicker, setShowingPicker] = React.useState(false);

    const {setFieldValue, ...ctx} = useFormikContext();
    const values = ctx.values as any;
    const value = parseDate(values[name]) || new Date();

    const onConfirm = (date?: Date) => {
        if (date != null) {
            const formattedDate = formatDate(date);
            setFieldValue(name, formattedDate);
        }
        hidePicker();
    };

    const hidePicker = () => setShowingPicker(false);

    return (
        <>
            <TouchableOpacity
                onPress={() => {
                    setShowingPicker(!showingPicker);
                }}>
                <TextInput
                    name={name}
                    placeholder={placeholder}
                    editable={false}
                    pointerEvents="none"
                />
            </TouchableOpacity>
            <DateTimePickerModal
                onConfirm={onConfirm}
                onCancel={hidePicker}
                locale={getLanguageTag()}
                isVisible={showingPicker}
                mode="date"
                date={value}
                display="spinner"
                headerTextIOS={placeholder}
                {...datePickerProps}
            />
        </>
    );
};

export default DatePicker;
