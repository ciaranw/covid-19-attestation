import React from 'react';
import {Formik} from 'formik';
import {useSelector} from 'react-redux';

import TextInput from './text-input';
import MultilineTextInput from './multiline-text-input';
import DatePicker from './date-picker';
import SubmitForm from './submit-form';
import {FormContainer, validateFormModel} from './form';
import Spacer from './spacer';
import SignatureControl from './signature-control';

import {translate} from '../i18n/index';
import {State} from '../redux/index';
import {UserDetailsState} from '../redux/user-details';
import UserDetailsModel from '../models/user-details';

type UserDetailsFormProps = {
    onSubmit: (values: UserDetailsState) => void;
    onAddSignature: () => void;
};

const UserDetailsForm: React.FC<UserDetailsFormProps> = ({
    onSubmit,
    onAddSignature,
}) => {
    const initialValues = useSelector<State, UserDetailsState>(
        state => state.UserDetails,
    );

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validate={validateFormModel(UserDetailsModel)}>
            <FormContainer>
                <TextInput
                    name="name"
                    placeholder={translate('UserDetails')('form')('name').t()}
                    autoCapitalize="words"
                    autoCompleteType="name"
                />
                <MultilineTextInput
                    name="address"
                    placeholder={translate('UserDetails')('form')(
                        'address',
                    ).t()}
                    autoCapitalize="words"
                    autoCorrect={false}
                />
                <DatePicker
                    name="dob"
                    placeholder={translate('UserDetails')('form')('dob').t()}
                />
                <SignatureControl onAddSignature={onAddSignature} />
                <Spacer />
                <SubmitForm
                    title={translate('UserDetails')('form')('next').t()}
                />
            </FormContainer>
        </Formik>
    );
};

export default UserDetailsForm;
