import {Text} from 'react-native';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';

export default styled(Text)`
    font-size: ${scale(24)}px;
    font-weight: 500;
    text-align: center;
`;
