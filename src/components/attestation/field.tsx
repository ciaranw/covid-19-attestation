import React from 'react';
import {Text, View} from 'react-native';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';

const Container = styled(View)`
    flex-direction: row;
    justify-content: flex-start;
    width: 100%;
    margin-bottom: ${scale(10)}px;
`;

const UnderlinedTextContainer = styled(View)`
    flex: 1;
`;

const Underline = styled(View)`
    width: 100%;
    height: 0.25px;
    border-color: rgb(128, 128, 128);
    border-width: 1px;
    border-style: dotted;
    border-radius: 1px;
`;

const UnderlinedText = styled(Text)`
    font-size: ${scale(12)}px;
`;

const Label = styled(Text)`
    font-size: ${scale(14)}px;
    margin-right: ${scale(10)}px;
    font-weight: 500;
`;

type FieldProps = {
    label: string;
    value?: string;
};

const AttestationField: React.FC<FieldProps> = ({label, value}) => (
    <Container>
        <Label>{label}</Label>
        {value != null && (
            <UnderlinedTextContainer>
                <UnderlinedText>{value}</UnderlinedText>
                <Underline />
            </UnderlinedTextContainer>
        )}
    </Container>
);

export default AttestationField;
