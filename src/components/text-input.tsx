import React from 'react';
import {
    TextInput as RNTextInput,
    TextInputProperties,
    View,
} from 'react-native';
import styled from 'styled-components';
import {useFormikContext} from 'formik';
import {scale} from 'react-native-size-matters';

import FormError from './form-error';

export type TextInputProps = {
    name: string;
    convertValue?: (value: any) => string;
} & TextInputProperties;

type TextValue = string | undefined;

const StyledTextInput = styled(RNTextInput)`
    padding: ${scale(10)}px;
    background-color: white;
    margin-bottom: ${scale(10)}px;
    font-size: ${scale(16)}px;
`;

const TextInput: React.FC<TextInputProps> = ({
    name,
    convertValue,
    ...textInputProps
}) => {
    const {handleChange, handleBlur, ...ctx} = useFormikContext();
    const values = ctx.values as any;
    let value: TextValue;
    if (convertValue != null) {
        try {
            value = convertValue(values[name]);
        } catch (err) {
            value = '';
        }
    } else {
        value = values[name] as TextValue;
    }

    return (
        <View>
            <StyledTextInput
                onChangeText={handleChange(name)}
                onBlur={handleBlur(name)}
                value={value}
                {...textInputProps}
            />
            <FormError name={name} />
        </View>
    );
};

export default TextInput;
