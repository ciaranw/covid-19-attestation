import React from 'react';
import {View, Image, Button} from 'react-native';
import styled from 'styled-components';
import {scale} from 'react-native-size-matters';
import {useSelector} from 'react-redux';

import {translate} from '../i18n/index';
import {State} from '../redux/index';

const SignatureImage = styled(Image)`
    width: ${scale(300)}px;
    height: ${scale(120)}px;
`;

type SignatureControlProps = {
    onAddSignature: () => void;
};

const SignatureControl: React.FC<SignatureControlProps> = ({
    onAddSignature,
}) => {
    const signatureImage = useSelector<State, string>(state => state.Signature);
    const buttonTitle =
        signatureImage !== ''
            ? translate('UserDetails')('form')('updateSignature').t()
            : translate('UserDetails')('form')('addSignature').t();
    return (
        <View>
            {signatureImage !== '' && (
                <SignatureImage
                    source={{uri: signatureImage}}
                    resizeMode="contain"
                />
            )}
            <Button title={buttonTitle} onPress={onAddSignature} />
        </View>
    );
};

export default SignatureControl;
