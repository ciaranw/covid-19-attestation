export type I18nValue = string;

export interface Translations {
    Startup: {
        title: I18nValue;
        subtitle: I18nValue;
    };
    UserDetails: {
        title: I18nValue;
        heading: I18nValue;
        form: {
            name: I18nValue;
            dob: I18nValue;
            address: I18nValue;
            addSignature: I18nValue;
            updateSignature: I18nValue;
            next: I18nValue;
        };
    };
    Signature: {
        title: I18nValue;
        done: I18nValue;
        clear: I18nValue;
    };
    Generate: {
        title: I18nValue;
        heading: I18nValue;
        activities: {
            work: I18nValue;
            shops: I18nValue;
            health: I18nValue;
            family: I18nValue;
            exercise: I18nValue;
        };
        missingActivity: I18nValue;
        warning: {
            title: I18nValue;
            message: I18nValue;
            ok: I18nValue;
            cancel: I18nValue;
        };
    };
    components: {
        MultilineTextInput: {
            done: I18nValue;
        };
        FormError: {
            invalid: I18nValue;
        };
    };
}
