import i18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';

import {Translations, I18nValue} from './types';
import en from './en';
import fr from './fr';

type TranslationMap = {
    [key: string]: Translations;
};

const translations: TranslationMap = {
    'en-GB': en,
    en,
    fr,
};

export const setI18nConfig = () => {
    const languageTag = getLanguageTag();

    // set i18n-js config
    i18n.translations = {[languageTag]: translations[languageTag]};
    i18n.locale = languageTag;
};

type Builder<T> = {
    <K extends keyof T>(
        key: T extends I18nValue ? never : K,
    ): T extends I18nValue ? never : Builder<T[K]>;
    t: T extends I18nValue
        ? (options?: i18n.TranslateOptions) => string
        : never;
};

const Builder = <T>(ctx?: string): Builder<T> => {
    const nested = function<K extends keyof T>(key: K): Builder<T[K]> {
        if (ctx == null) {
            return Builder(`${key}`);
        } else {
            return Builder(`${ctx}.${key}`);
        }
    };
    if (ctx != null) {
        nested.t = (options?: i18n.TranslateOptions) => i18n.t(ctx, options);
    }
    return nested as Builder<T>;
};

export const translate = Builder<Translations>();

export const getLanguageTag = () => {
    const fallback = {languageTag: 'fr'};

    const {languageTag} =
        RNLocalize.findBestAvailableLanguage(Object.keys(translations)) ||
        fallback;

    return languageTag;
};
