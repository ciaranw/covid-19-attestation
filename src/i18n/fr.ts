import {Translations} from './types';

const Fr: Translations = {
    Startup: {
        title: 'COVID-19',
        subtitle: 'Attestation',
    },
    UserDetails: {
        title: 'Profil',
        heading:
            'Veuillez saisir votre nom complet, votre date de naissance et votre adresse personnelle',
        form: {
            name: 'Nom et Prenom',
            dob: 'Né(e) le',
            address: 'Demeurant',
            addSignature: 'Ajouter une Signature',
            updateSignature: 'Changer la Signature',
            next: 'Suivant',
        },
    },
    Signature: {
        title: 'Signature',
        done: 'OK',
        clear: 'Réinitialiser',
    },
    Generate: {
        title: 'Générer une Attestation',
        heading: 'Veuillez choisir votre activité',
        activities: {
            work:
                'Déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle, lorsqu’ils sont indispensables à l’exercice d’activités ne pouvant être organisées sous forme de télétravail (sur justificatif permanent) ou déplacements professionnels ne pouvant être différés',
            shops:
                'Déplacements pour effectuer des achats de première nécessité dans des établissements autorisés (liste sur gouvernement.fr)',
            health: 'Déplacements pour motif de santé ',
            family:
                'Déplacements pour motif familial impérieux, pour l’assistance aux personnes vulnérables ou la garde d’enfants',
            exercise:
                'déplacements brefs, à proximité du domicile, liés à l’activité physique individuelle des personnes, à l’exclusion de toute pratique sportive collective, et aux besoins des animaux de compagnie.',
        },
        missingActivity: 'Please choose an activity.',
        warning: {
            title: 'Regardez-vous!',
            message:
                "Cette application ne doit être utilisée qu'en cas d'urgence. Veuillez envisager de rester chez vous pour arrêter la propagation de COVID-19.",
            ok: 'Je Comprends',
            cancel: 'Annuler',
        },
    },
    components: {
        MultilineTextInput: {
            done: 'OK',
        },
        FormError: {
            invalid: "Cette entrée n'est pas valide",
        },
    },
};

export default Fr;
