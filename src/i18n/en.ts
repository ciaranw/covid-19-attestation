import {Translations} from './types';

const En: Translations = {
    Startup: {
        title: 'COVID-19',
        subtitle: 'Attestation',
    },
    UserDetails: {
        title: 'User Details',
        heading: 'Please enter your full name, date of birth and home address',
        form: {
            name: 'Name',
            dob: 'Date of Birth',
            address: 'Full Address',
            addSignature: 'Add Signature',
            updateSignature: 'Update Signature',
            next: 'Next',
        },
    },
    Signature: {
        title: 'Please sign above',
        done: 'Done',
        clear: 'Clear',
    },
    Generate: {
        title: 'Generate Attestation',
        heading: 'Please choose your activity',
        activities: {
            work:
                'Travel between your home and place of work, where remote working cannot be achieved',
            shops:
                'Trips to make essential purchases in authorised establishments',
            health: 'Travel for health reasons',
            family: 'Travel for assistance of vunerable persons or childcare',
            exercise:
                'A short excursion, close to your home, for the purpose of physical activity (excluding group activity) or excercise of pets.',
        },
        missingActivity: 'Please choose an activity.',
        warning: {
            title: 'Please Read!',
            message:
                'This app should only be used for emergencies only. Please consider staying in your home to stop the spread of COVID-19.',
            ok: 'I Understand',
            cancel: 'Cancel',
        },
    },
    components: {
        MultilineTextInput: {
            done: 'Done',
        },
        FormError: {
            invalid: 'This field is invalid',
        },
    },
};

export default En;
