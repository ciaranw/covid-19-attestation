import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import codePush, {CodePushOptions} from 'react-native-code-push';
import Icon from 'react-native-vector-icons/FontAwesome';
Icon.loadFont();

import RootNavigator from './navigation/root';
import {setI18nConfig} from './i18n/index';
import store from './redux/index';
import {persistor} from './redux/persist';

function App() {
    React.useEffect(setI18nConfig, []);

    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <NavigationContainer>
                    <RootNavigator />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    );
}

const codePushOptions = {
    checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
    installMode: codePush.InstallMode.ON_NEXT_SUSPEND,
    ignoreFailedUpdates: false,
} as CodePushOptions;

export default codePush(codePushOptions)(App);
