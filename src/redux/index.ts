import {store} from './persist';

export type State = ReturnType<typeof store.getState>;

export default store;
