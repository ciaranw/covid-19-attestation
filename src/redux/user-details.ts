import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {TypeOf} from 'io-ts';

import UserDetailsModel from '../models/user-details';

export type UserDetailsState = TypeOf<typeof UserDetailsModel>;

export type UpdateActionPayload = {
    name: string;
    dob: string;
    address: string;
};

const slice = createSlice({
    name: 'UserDetails',
    initialState: {name: '', address: '', dob: ''} as UserDetailsState,
    reducers: {
        update: (state, action: PayloadAction<UpdateActionPayload>) => {
            state.name = action.payload.name;
            state.dob = action.payload.dob;
            state.address = action.payload.address;
        },
    },
});

export const {update} = slice.actions;

export default slice.reducer;
