import {createSlice, PayloadAction} from '@reduxjs/toolkit';

const slice = createSlice({
    name: 'Signature',
    initialState: '' as string,
    reducers: {
        update: (state, action: PayloadAction<string>) => {
            return action.payload;
        },
    },
});

export const {update} = slice.actions;

export default slice.reducer;
