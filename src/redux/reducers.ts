import {combineReducers} from '@reduxjs/toolkit';
import UserDetails from './user-details';
import Signature from './signature';
import Activity from './activity';

export default combineReducers({
    UserDetails,
    Signature,
    Activity,
});
