import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type ActivityState = {
    selected?: string;
};

const slice = createSlice({
    name: 'Activity',
    initialState: {selected: undefined} as ActivityState,
    reducers: {
        update: (state, action: PayloadAction<string>) => {
            state.selected = action.payload;
        },
        reset: state => {
            state.selected = undefined;
        },
    },
});

export const {update, reset} = slice.actions;

export default slice.reducer;
