import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import {name} from '../../app.json';

import reducers from './reducers';

const persistConfig = {
    key: name,
    storage: AsyncStorage,
    whitelist: ['UserDetails', 'Signature'],
};

export const reducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
    reducer,
    middleware: getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
    }),
});
export const persistor = persistStore(store);
